# module has changed

Keywords: %module has changed

This message is emitted by the build system when a module has changed. It
indicates that the module will be compiled again. It is a purely informational
message and can be ignored.

## Solutions

N/A

## Examples

N/A
