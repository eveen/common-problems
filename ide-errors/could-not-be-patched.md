# could not be patched

Keywords: %could not be patched%

This error occurs when using the Clean IDE or `cpm`, `_system.abc` is removed
or read-only, and profiling settings are changed. In this case, the IDE and
`cpm` rewrite a header in the ABC file. When the file is not present or not
writeable, the error appears.

## Solutions

- Place back `_system.abc` and make sure it is writeable. The easiest way is to
  reinstall Clean. Alternatively, you can get the file from
  `https://svn.cs.ru.nl/repos/clean-libraries/trunk/Libraries/StdEnv/Clean%20System%20Files/_system.abc`
  (32-bit) or
  `https://svn.cs.ru.nl/repos/clean-libraries/trunk/Libraries/StdEnv/StdEnv%2064%20Changed%20Files/_system.abc`
  (64-bit), and place it in `StdEnv/Clean System Files`.

## Examples

N/A
