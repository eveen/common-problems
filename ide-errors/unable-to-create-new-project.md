# Unable to create new project

Keywords: %unable to create new project%; %no active module%

This IDE error occurs when you are trying to create a project when there is no
active module window. The IDE requires a main module to create a project.

## Solutions

- First open or create the main module.

## Examples

N/A
