# Unable to open Errors file

Keywords: %Unable to open Errors%

This error should not normally occur and indicates a corrupt installation.

## Solutions

- Reinstall Clean. Make sure that file permissions are set sensibly.

## Examples

N/A
