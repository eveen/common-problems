# conflicting kinds

Keywords: %conflicting kinds%

This error occurs when a type is used to instantiate a type variable with
another kind, or when a type variable is used with different ununifiable kinds.

Imprecisely, the kind of a type refers to whether it is a 'full' type or not.
For instance, `Int`, `Bool` and `[a]` are all 'full' types, in that there can
be values of these types (e.g., `37`, `True` and `[1,2,3]`). But `Maybe` is not
a 'full' type, since there are no values of this type (e.g., `Just 37` is of
type `Maybe Int`, not `Maybe`).

(More precisely, the kind of a type refers to the number and kind of its
variables, and can be seen as the type of a type.)

Usually, the error indicates that you forgot a type argument (see the first two
examples below), although it can also occur in more complex contexts (see the
last example below).

## Solutions

- Carefully check the function types.

## Examples

```clean
import StdMaybe
f :: Maybe -> Maybe
f x = x
```

Here, `Maybe` is used as a 'full' type (kind `*`, like `Int`, etc.). However,
the type definition is `:: Maybe a = Just a | Nothing`. It has a type variable;
its actual kind is `*->*` and 'full' types using `Maybe` are for example
`Maybe Int`. Hence, a correct type for `f` is `f :: (Maybe a) -> Maybe a` (or,
of course, `f :: a -> a` in this case).

---

```clean
import StdEnv, StdMaybe
instance + Maybe
where
	(+) (Just x) (Just y) = Just (x+y)
	(+) _        _        = Nothing
```

Here, `Maybe` is used to instantiate the variable `a` in
`class + a :: a a -> a`. But from the type of `+` one can derive that `a` must
be a 'full' type (kind `*`) there. `Maybe` is not a 'full' type since it has a
type variable. A correct instance header for this instance would be:

```clean
instance + (Maybe a) | + a
```

---

```clean
f :: (a b) -> a
```

Here, from the usage `(a b)` one can derive that `a` is not a 'full' type
(it has kind `k->*` where `b :: k`). But `a` is also the result type of the
function, so it also must be a 'full' type (kind `*`). Hence, there is no `a`
possible that adheres to this definition; the type is meaningless.
