# X could not be imported

Keywords: %could not be imported%

This error occurs when an imported module could not be found. The module name
should *not* contain the library (e.g. `StdEnv` or `Platform`) and should *not*
contain the extension (`.dcl` or `.icl`). Examples of frequently used module
names are `StdEnv` and `Data.Maybe`.

While modules in the StdEnv library are automatically searched by Clean build
tools, other libraries need to be added to the search paths before use. Check
the documentation of your build tool (the IDE, `clm`, `cpm`, etc.) on how to do
this.

## Solutions

- Check the spelling of the module name, and make sure it does not contain the
  library name or the file extension.
- Make sure that your build tool (the IDE, `clm` or `cpm`) can find the right
  files by giving it the right search paths.

## Examples

N/A
