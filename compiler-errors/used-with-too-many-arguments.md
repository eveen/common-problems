# used with too many arguments

Keywords: %too many arguments%

This error occurs when a constructor is used with too many arguments.

## Solutions

- Check the usage of the constructor in the line referenced in the error.

## Examples

```clean
import StdMaybe

Start = Just 1 2
```

Here, `Just :: a -> Maybe a` can accept only one argument, but two are given.
Correct Start rules are `Just 1` and `Just 2`, for example.
