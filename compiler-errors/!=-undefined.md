# != undefined

Keywords: != undefined; !=

You probably used `!=` as the boolean infix operator to test for inequality. 
In Clean you must use the infix operator `<>` for this purpose. 

## Solutions

- Replace `!=` with `<>`, and make sure to add `import StdEnv` in your module.

## Examples

```clean
divide x y
| y != 0 = [x / y]
| x == 0 = []
```

Here you can fix the problem by using `<>` instead of `!=`:

```clean
divide x y
| y <> 0 = [x / y]
| x == 0 = []
```
