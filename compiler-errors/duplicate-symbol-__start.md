# duplicate symbol __start

Keywords: duplicate symbol%

This error is generated when multiple object files are linked together,
containing symbols with the same names, and no optimizing linker is used.

For example, this problem occurs on Mac when you import modules containing a
`Start` rule.

## Solutions

- Remove the `Start` function from the imported module.
- Use a system with a linker that strips these symbols (e.g. linux).
- Do not use the `clm` flag `-no-opt-link`, which turns off the optimizing
  linker.

## Examples

N/A
