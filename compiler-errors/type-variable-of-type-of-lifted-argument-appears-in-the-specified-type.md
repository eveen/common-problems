# type variable of type of lifted argument appears in the specified type

Keywords: %type of lifted argument%

This error occurs when the type of a local definition (for instance, in a
`where` block) depends on the type of the function it is a part of.

## Solutions

- The easiest solution is usually to remove the illegal type definition of the
  local function.

## Examples

```clean
app :: (a -> b) a -> b
app f x = y
where
	y :: b
	y = f x
```

Here, it seems as if `y` can have 'any' type, while in fact it depends on the
type of `f`. The error is resolved by removing the type signature of `y`.
