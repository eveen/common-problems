# first (second) argument of infix operator missing

Keywords: %argument of infix operator%

This error is caused by a usage of an infix operator in a prefix or postfix
notation.

Note that expressions like `(42 +)` or `(+ 37)` are partial applications in
Haskell but illegal in Clean. Use `((+) 42)` and `(flip (+) 37)` or
`(\x -> x + 37)` respectively.

## Solutions

- Give the infix operator the required argument
- To pass an infix operator in prefix style, wrap it in parentheses.

## Examples

```clean
import StdEnv
Start = map (* 5) [1..10]
```

Here, `(* 5)` is illegal but can be replaced by `(flip (*) 5)`. Due to
commutativity of integer multiplication `((*) 5)` is functionally (though not
syntactically) equivalent.
