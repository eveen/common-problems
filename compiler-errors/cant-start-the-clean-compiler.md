# can't start the clean compiler

Keywords: %start the clean compiler%

This error indicates a corrupt installation. It should not normally occur.

## Solutions

- Reinstall Clean, following the installation instruction meticulously.
- If you are enrolled in a programming course, contact one of the instructors
  with steps to reproduce the issue.
- Contact `clean-bugs@cs.ru.nl` with details about your installation and how to
  reproduce the error.

## Examples

N/A
