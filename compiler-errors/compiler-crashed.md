# Compiler crashed

Keywords: %Compiler crashed%; %reading compiler result failed%

This error should not normally occur and indicates a corrupt installation or a
bug.

Depending on the platform and the exact cause of the crash, the error message
can take several forms:

- `Compiler crashed`
- `reading compiler result failed`

## Solutions

- If you are enrolled in a programming course, contact one of the instructors
  with steps to reproduce the issue.
- Contact `clean-bugs@cs.ru.nl` with details about your installation and how to
  reproduce the error.

## Examples

N/A
