# used with wrong arity (class member)

Keywords: %used with wrong arity%

This error occurs when a class instance has an incorrect number of arguments (arity).

## Solutions

- Carefully check the number of parameters (pattern matches) of the function definition.

## Examples

```clean
:: Gender = Male | Female

instance == Gender
where
	== _ = False   // this function has one parameter instead of two
```
