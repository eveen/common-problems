# Not enough memory to allocate heap and stack

Keywords: %Not enough memory%

This error occurs when one attempts to run a Clean program with more heap
and/or stack space than the operating system can provide.

## Solutions

- Run the program with less heap and/or stack space.
- If you really need the heap you can enable memory overcommitting on linux.
- If you really need the stack space you can increase the OS stack limit on
  POSIX using `ulimit`.

## Examples

N/A
