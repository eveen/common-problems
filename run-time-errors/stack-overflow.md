# Stack overflow

Keywords: stack overflow

This can happen when there is some infinite recursion in your program and the
stack becomes too large.

## Solutions

- If there is an infinite recursion, you need to change the program to remove
  it.

- Otherwise, it may be that you just need a little bit more stack.
  You can add stack with `-s 2M` (for instance) on the command line or by
  changing the `StackSize` setting in your project file. In the IDE you can
  enlarge the stack from the application settings.

- If this occurs while compiling code, try increasing the compiler stack size.
  This can be done with the `-aC` flag in `clm`, in the environment using `cpm`
  or the IDE. See {{Not enough memory to allocate heap and stack}} to increase
  the stack limit for the OS. If the problem remains it means that the
  compiler crashed due to a bug. Try to reduce the code to a MWE (minimal
  working example) and report the compiler bug to `clean@cs.ru.nl` or the Clean
  mailing list.

## Examples

```clean
Start = foldr (+) 0 (repeat 1)
```

This is an endless recursion, since `repeat` generates an infinite list.

---

```clean
Start = foldr (+) 0 (repeatn 100000 1)
```

This does not work with the default 500kB stack, but does work with a 1MB
stack.
