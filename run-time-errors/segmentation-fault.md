# Segmentation fault

Keywords: %segmentation fault%; segfault

This error should not normally occur. However, it may pop up when your program
uses low-level features (unchecked array indices; linking with C; custom ABC
code).

On some installations, a segmentation fault can be a symptom of a
{{stack overflow}} or a {{heap full}.

When a program is compiled without stack index checking, an
{{index out or range}} error may masquerade as a segmentation fault.

If this occurs while compiling code, it means that the compiler crashed.
Try to reduce the code to a MWE (minimal working example) and report the
compiler bug to `clean@cs.ru.nl` or the Clean mailing list.

## Solutions

- If you are enrolled in a programming course, contact one of the instructors
  with steps to reproduce the issue.
- Contact `clean-bugs@cs.ru.nl` with details about your installation and how to
  reproduce the error.

## Examples

N/A
