#!/usr/bin/env python3
import glob
import json
from os.path import basename
import re
import sys

failure = False
seen_files = {}

def error(msg):
    global failure
    failure = True
    print('\033[0;31mError: ' + msg + '\033[0m')

def error_append(msg):
    print('\033[0;33m' + msg + '\033[0m')

def make_keyword(keyword):
    return re.escape(keyword.strip())\
            .replace(r'\%', r'.*')\
            .replace(r'\#', r'\d+')

def extract_keywords(lines):
    keywords = [l[10:-1].split(';') for l in lines if l[:10] == 'Keywords: ']
    keywords = sum(keywords, [])
    keywords = [make_keyword(kw) for kw in keywords]
    return keywords

def extract_description(lines):
    desc = ''

    reading = False
    for l in lines:
        if l[:3] == '## ':
            break
        if reading:
            desc += l
        if l[:10] == 'Keywords: ':
            reading = True

    return desc.strip()

def extract_solutions(lines):
    sols = []

    reading = False
    for l in lines:
        if l[:-1] == '## Examples':
            break
        if reading:
            if l[:2] == '- ':
                sols.append(l[2:-1])
            elif l[:2] == '  ':
                sols[-1] += ' ' + l[2:-1]
            elif l == 'N/A\n':
                pass
            elif not re.match(r'^\s*$', l):
                error(l)
        if l[:-1] == '## Solutions':
            reading = True

    return [sol.strip() for sol in sols]

def extract_examples(lines):
    exs = ['']

    reading = False
    for l in lines:
        if reading:
            if l[:-1] == '---':
                exs.append('')
            else:
                exs[-1] += l
        if l[:-1] == '## Examples':
            reading = True

    return [ex.strip() for ex in exs if ex != '']

def index(fname):
    with open(fname, 'r') as f:
        print('Indexing {}...'.format(fname))
        lines = f.readlines()

        titles = []
        for line in lines:
            if line[0] != '#':
                break
            titles.append(line[1:].strip())
        seen_files[fname] = {t: False for t in titles}

        keywords = extract_keywords(lines)
        description = extract_description(lines)
        solutions = extract_solutions(lines)
        examples = extract_examples(lines)

        if len(keywords) == 0:
            error('no keywords found')
        if description == '':
            error('no description found')
        if len(solutions) == 0 and fname[:18] != 'compiler-messages/':
            error('no solutions found')
        if len(examples) == 0:
            error('no examples found')

        examples = [ex for ex in examples if ex != 'N/A']

        return {
            'key': fname[:-3],
            'title': '; '.join(titles),
            'keywords': keywords,
            'description': description,
            'solutions': solutions,
            'examples': examples
            }

def check_readme():
    with open('README.md', 'r') as f:
        print('Checking README...')
        lines = f.readlines()

        for line in lines:
            match = re.match(r'- \[(.*)\]\((.*)\)', line)
            if match is None:
                continue
            title = match.group(1)
            fname = match.group(2)

            if fname[0] != '/':
                error('file name \'{}\' should start with /'.format(fname))
                continue
            fname = fname[1:]
            if fname not in seen_files:
                error('file \'{}\' was referenced but is not indexed - '
                      'does it exist?)'.format(fname))
                continue
            if title not in seen_files[fname]:
                error('title \'{}\' not found in \'{}\'; it has the titles:'\
                        .format(title, fname))
                for title in seen_files[fname]:
                    error_append('\t\'{}\''.format(title))
                continue
            seen_files[fname][title] = True

        for fname, titles in seen_files.items():
            for title, seen in titles.items():
                if not seen:
                    error('problem not referenced in readme: \'{}\' in \'{}\''\
                            .format(title, fname))


if __name__ == '__main__':
    problems = []

    for f in glob.glob('*/*.md'):
        problems.append(index(f))

    check_readme()

    with open('common-problems.json', 'w') as f:
        f.write(json.dumps(problems))

    if failure:
        sys.exit(1)
